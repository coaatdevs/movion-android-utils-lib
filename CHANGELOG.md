## Changelog

### 0.1.10

- Añadidos nuevos métodos para comprobar si dos fechas tienen el mismo mes

### 0.1.9

- Añadidos nuevos métodos de lectura de ficheros

### 0.1.8

- Sobrecargado método para pasar una imagen a base64 para poder elegir el formato de compresión de la imagen (PNG, JPEG, etc)

### 0.1.7

- Añadido métodos a la clase de utilidades para recuperar el IMEI y la dirección MAC de un dispositvo
- Añadida nueva clase PermissionsUtils para controlar la habilitación de permisos en la aplicación

### 0.1.6

- Añadido método para pasar una imagen a base64

### 0.1.5

- Incluido filtro para campos de tipo EditText para evitar la entrada de caracteres inválidos usando una regex
- Incluido filtro para campos numéricos de modo que se puede especificar el número de digitos válidos tanto para la parte entera como para la parte decimal

### 0.1.4

- Incluida clases ExcludeGson y CustonGsonExclusionStrategy para permitir excluir clases y propiedades de una clase de
la serializacion/deserializacion de un objeto usando Gson


### 0.1.3

- Incluida clase ViewUtils

### 0.1.2

- Incluido serializador/deserializador de fechas con un formato concreto para Gson

### 0.1.1

- Actualizadas variables de configuracion de gradle para subida de repositorio para que esté a nivel de libreria y no proyecto global
- Añadido método constantes y métodos utilizados en Coaat en DateTools
- Agregada librería EmailValidator usada en el Cooat
- Actualizado FileUtils con algunos métodos necesarios en coaat
- Actualizado ImageUtils con algunos métodos necesarios en coaat
- Añadida clase MetricsLogs a la librería
- Actualizada clase StringUtils con métodos usados en el Coaat

### 0.1.0

- Añadidas clases de utilidades iniciales: 
    - AppLog
    - ColorUtils
    - ComponentUtils
    - DateTools
    - DeviceUtils
    - FileUtils
    - GeneralUtils
    - ImageUtils
    - MimeTypes
    - NumberFormatter
    - PersinstentHandler
    - SharedPreferencesUtil
    - StringUtils