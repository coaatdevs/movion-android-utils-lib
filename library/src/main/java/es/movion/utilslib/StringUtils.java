package es.movion.utilslib;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Clase de utilidades relacionadas con cadenas de texto
 */
public class StringUtils {

    public static final String LINE_FEED_CHARACTER = "\n";
    public static final String CARRIAGE_RETURN_CHARACTER = "\r";
    public static final String AEIOUY = "AEIOUY";
    private static final String UPPERCASE_ASCII =
            "AEIOU" // grave
                    + AEIOUY // acute
                    + AEIOUY // circumflex
                    + "AO" // tilde
                    + AEIOUY // umlaut
                    + "A" // ring
                    + "C" // cedilla
                    + "OU" // double acute
            ;
    private static final String UPPERCASE_UNICODE =
            "\u00C0\u00C8\u00CC\u00D2\u00D9"
                    + "\u00C1\u00C9\u00CD\u00D3\u00DA\u00DD"
                    + "\u00C2\u00CA\u00CE\u00D4\u00DB\u0176"
                    + "\u00C3\u00D5"
                    + "\u00C4\u00CB\u00CF\u00D6\u00DC\u0178"
                    + "\u00C5"
                    + "\u00C7"
                    + "\u0150\u0170";


    private StringUtils() {
    }

    /**
     * Limpia espacios en blanco duplicados
     *
     * @param origen cadena de origen
     * @return cadena de origen limpia
     */
    public static String cleanEndLineCharacters(String origen) {
        String cleanString = origen.replaceAll(LINE_FEED_CHARACTER, "");
        cleanString = cleanString.replaceAll(CARRIAGE_RETURN_CHARACTER, "");
        return cleanString;
    }

    /**
     * Limpia espacios en blanco duplicados
     *
     * @param origen cadena de origen
     * @return cadena de origen limpia
     */
    public static String removeDuplicateWhiteSpaces(String origen) {
        return origen.replaceAll("\\s+", " ");
    }


    /**
     * Busca en origen coincidencias de patron y las reemplaza con sustituto.
     *
     * @param patron    de búsqueda
     * @param sustituto cadena por la que se va a reemplazar
     * @param origen    cadena de origen donde hacer el reemplazamiento
     * @return cadena limpia
     */
    public static String pregReplace(String patron, String sustituto, String origen) {
        return origen.replaceAll(patron, sustituto);
    }

    /**
     * Limpia de espacios la cadena origen y busca en origen coincidencias de patron y las reemplaza con sustituto.
     *
     * @param patron    de búsqueda
     * @param sustituto cadena por la que se va a reemplazar
     * @param origen    cadena de origen donde hacer el reemplazamiento
     * @return cadena limpia
     */
    public static String cleanAndPregReplace(String patron, String sustituto, String origen) {
        return pregReplace(patron, sustituto, removeDuplicateWhiteSpaces(origen).trim());
    }

    /**
     * Comprueba si el patron está dentro del origen y ambos no son nulos
     *
     * @param patron
     * @param origen
     * @return devuelve true si la cadena contiene el patron, false en otro caso
     */
    public static boolean stringContains(String patron, String origen) {
        return (origen != null) && (patron != null) && origen.contains(patron);
    }

    /**
     * Modifica la cadena de entrada para remover los acentos y transformar en mayúsculas
     *
     * @param txt cadena de entrada
     * @return cadena transmformada
     */
    public static String toUpperCaseSansAccent(String txt) {
        if (txt == null) {
            return null;
        }
        String txtUpper = txt.toUpperCase();
        StringBuilder sb = new StringBuilder();
        int n = txtUpper.length();
        for (int i = 0; i < n; i++) {
            char c = txtUpper.charAt(i);
            int pos = UPPERCASE_UNICODE.indexOf(c);
            if (pos > -1) {
                sb.append(UPPERCASE_ASCII.charAt(pos));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * Lee el contenido de un stream y lo convierte a cadena de caracteres
     *
     * @param inStream inputStream
     * @return devuelve una cadena de caracteres
     */
    public static String readString(InputStream inStream) {
        String response = null;
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(inStream));
            StringBuilder builder = new StringBuilder();
            String aux;

            while ((aux = reader.readLine()) != null) {
                builder.append(aux);
            }
            response = builder.toString();
        } catch (IOException e) {
            AppLog.e("Error al leer inputStream de la respuesta", e);
        } finally {
            GeneralUtils.closeStream(reader);
            GeneralUtils.closeStream(inStream);
        }

        return response;
    }

    public static int getStringFromResources(Context context, String resName) {
        return context.getResources().getIdentifier(resName, "string", context.getPackageName());
    }
}
