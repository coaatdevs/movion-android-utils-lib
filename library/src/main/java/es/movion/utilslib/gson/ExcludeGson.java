package es.movion.utilslib.gson;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Todos los fields y clases marcados como exclude no se tendrán en cuenta para la serialización/deserializacion de gson
 * <p>
 * Para ello habrá que utilizar la estrategia de exclusion {@link CustomGsonExclusionStrategy}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface ExcludeGson {

}
