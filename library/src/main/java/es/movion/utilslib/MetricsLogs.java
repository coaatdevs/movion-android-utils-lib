package es.movion.utilslib;

public class MetricsLogs {

    private long startTime;

    public MetricsLogs() {
        reset();
    }

    public void reset() {
        startTime = System.currentTimeMillis();
    }

    public void logStep(String tag, String message) {
        long finishTime = System.currentTimeMillis() - startTime;
        AppLog.d(tag, message + "'=" + finishTime + "ms");
        reset();
    }
}
