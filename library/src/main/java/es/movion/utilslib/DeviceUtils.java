package es.movion.utilslib;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import java.util.UUID;

/**
 * Device utils
 */
public class DeviceUtils {

    private static final String TAG = "DeviceUtils";

    private DeviceUtils() {
        // Object can't be instanciated
    }

    /**
     * Determine if the device is a tablet (i.e. it has a large screen).
     *
     * @param context The calling context.
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }

    /**
     * Get current displayed version
     */
    public static String getVersionName(Context context) {
        String version = "";
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            AppLog.e(TAG, e.getMessage());
        }
        return version;
    }

    /**
     * Get current code version
     */
    public static String getVersionCode(Context context) {
        String versionCode = "";
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = String.valueOf(pInfo.versionCode);
        } catch (Exception e) {
        }
        return versionCode;
    }

    /**
     * Get the device name
     *
     * @return the device name
     */
    public static String getDeviceName() {
        return Build.MODEL;
    }

    /**
     * Get the device manufacturer
     *
     * @return the device manufacturer
     */
    public static String getDeviceManufacturer() {
        return Build.MANUFACTURER;
    }

    /**
     * Get the device operative system version
     *
     * @return device os version
     */
    public static String getDeviceOsVersion() {
        return Build.VERSION.RELEASE;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static String getRadioBand() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return Build.getRadioVersion();
        } else {
            return Build.RADIO;
        }
    }

    /**
     * Get the device's IMEI
     *
     * @param context
     * @return the imei or empty string
     */
    public static String getIMEI(Context context) {
        String imei = "";
        if (PermissionsUtils.isGranted(context, Manifest.permission.READ_PHONE_STATE)) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null) {
                imei = tm.getDeviceId();
                imei = imei != null ? imei.trim() : "";
            }
        }

        return imei;
    }

    /**
     * Get the device's mac address
     * <p>
     * Note: Requires android.permission.ACCESS_WIFI_STATE
     *
     * @param context
     * @return the mac adddress or empty string;
     */
    public static String getMacAddress(Context context) {
        String macAddress = "";
        if (PermissionsUtils.isGranted(context, Manifest.permission.ACCESS_WIFI_STATE)) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (wifiManager != null && wifiManager.getConnectionInfo() != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                macAddress = wifiManager.getConnectionInfo().getMacAddress();
                macAddress = macAddress != null ? macAddress.replaceAll(":", "").trim() : "";
            }
        }
        return macAddress;
    }

    /**
     * Returns the device id (usually IMEI o MAC Address)
     * <p>
     * Note: Requires android.permission.READ_PHONE_STATE
     *
     * @param context associated context
     * @return device id if exits or an empty string in other case
     */
    public static String getDeviceId(Context context) {
        String deviceId = getIMEI(context);
        if (GeneralUtils.isNullOrEmpty(deviceId))
            deviceId = getMacAddress(context);

        return deviceId;
    }

    /**
     * Generates a universal unique id for the current device
     * <p>
     * Note: Requires android.permission.READ_PHONE_STATE and android.permission.ACCESSS_WIFI_STATE
     *
     * @param context the calling context
     * @return universal unique id for the current device
     */
    public static String getDeviceUUID(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId, simSerialNumber, androidId;
        deviceId = "" + tm.getDeviceId();
        simSerialNumber = "" + tm.getSimSerialNumber();
        androidId = ""
                + android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        UUID uuid = new UUID(androidId.hashCode(), ((long) deviceId.hashCode() << 32) | simSerialNumber.hashCode());

        return uuid.toString();
    }


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }


    /**
     * Returns the current screen dimeensions
     *
     * @param context context to get the display metrics values
     * @return an integer array that contains the width and height values
     */
    public static int[] getScreenDimension(Activity context) {
        DisplayMetrics displayM = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayM);

        int width = displayM.widthPixels;
        int height = displayM.heightPixels;

        int[] dimensions = {width, height};
        return dimensions;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

}
