package es.movion.utilslib;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.io.StreamCorruptedException;

/**
 * Clase de utilidades para guardar objetos en memoria persistente a nivel de aplicación.
 * 
 * @author hrahal
 * @author gtirado
 */
public class PersistentHandler {

	private static final String lOG_TAG = PersistentHandler.class.getSimpleName();

	/**
	 * Recupera un objeto de un fichero persistente
	 * 
	 * @param context contexto en el que se realiza la llamada
	 * @param filename nombre del archivo de persistencia que guardará la información del objeto
	 * @return el objeto recuperado desde persistencia o null si no lo ha conseguido encontrar
	 */
	public static Object getObject(Context context, String filename) {
		Object obj = null;
		FileInputStream fis = null;
		ObjectInputStream is = null;

		try {
			fis = context.openFileInput(filename);
			is = new ObjectInputStream(fis);
			obj = is.readObject();
		} catch (FileNotFoundException e) {
			AppLog.w(lOG_TAG, "Fichero no encontrado getObject() '" + filename + "'", e);
		} catch (StreamCorruptedException e) {
			AppLog.e(lOG_TAG, "Error getObject StreamCorrupted.", e);
		} catch (OptionalDataException e) {
			AppLog.e(lOG_TAG, "Error getObject OptionalData.", e);
		} catch (IOException e) {
			AppLog.e(lOG_TAG, "Error getObject IOException.", e);
		} catch (ClassNotFoundException e) {
			AppLog.e(lOG_TAG, "Error getObject ClassNotFoundException: ", e);
		} finally {
			GeneralUtils.closeStream(is);
			GeneralUtils.closeStream(fis);
		}

		return obj;
	}

	/**
	 * Guarda un objeto en el fichero pasado por parámetro
	 * 
	 * @param context contexto en el que se realiza la llamada
	 * @param obj objeto que se pretende persistir
	 * @param filename nombre del fichero que alojará el objeto
	 */
	public static void saveObject(Context context, Serializable obj, String filename) {
		FileOutputStream fos = null;
		ObjectOutputStream os = null;
		try {
			fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
			os = new ObjectOutputStream(fos);
			os.writeObject(obj);
		} catch (FileNotFoundException e) {
			AppLog.e(lOG_TAG, "Error saveObject FileNotFoundException", e);
		} catch (IOException e) {
			AppLog.e(lOG_TAG, "Error saveObject IOException", e);
		} finally {
			GeneralUtils.closeStream(os);
			GeneralUtils.closeStream(fos);
		}
	}

	/**
	 * Elimina el fichero persistente asociado a un objeto guardado
	 * 
	 * @param context contexto en el que se realiza la llamada
	 * @param filename nombre del fichero que alojaraba el objeto
	 */
	public static void deleteObject(Context context, String filename) {
		FileOutputStream fos = null;
		context.deleteFile(filename);
	}
}
