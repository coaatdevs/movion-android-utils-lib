package es.movion.utilslib;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Utilidades para comprobar que permisos de aplicación
 */
public class PermissionsUtils {


    private PermissionsUtils() {
        // Objeto no instanciable
    }

    public static boolean isGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isAllGranted(Context context, String[] permissions) {
        boolean allGranted = true;
        if (permissions == null || permissions.length == 0)
            return false;
        else {
            for (int i = 0; i < permissions.length; i++) {
                allGranted = allGranted && isGranted(context, permissions[i]);
            }
        }

        return allGranted;
    }
}
