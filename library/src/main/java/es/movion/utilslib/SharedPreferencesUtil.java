package es.movion.utilslib;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;


/**
 * Clase de utilidades para guardar objetos en shared preferences a nivel de aplicación.
 *
 * @author gtirado
 */
public class SharedPreferencesUtil {

    private static Context appContext;

    private SharedPreferencesUtil() {
    }


    public static Context getAppContext() {
        return appContext;
    }

    public static void setAppContext(Context appContext) {
        SharedPreferencesUtil.appContext = appContext;
    }

    /**
     * Carga desde persistencia un objeto a partir de su clave
     *
     * @param key      clave con la que se va a persistir
     * @param classOfT clase del objeto
     * @param <T>      tipo delo objeto
     * @return el objeto o null
     * @throws JsonSyntaxException
     */
    public static <T> T loadObject(String key, Class<T> classOfT) throws JsonSyntaxException {
        return loadObject(key, classOfT, new Gson());
    }

    /**
     * Carga desde persistencia un objeto a partir de su clave
     *
     * @param key       clave con la que se va a persistir
     * @param objetType tipo del objeto
     * @param <T>       tipo delo objeto
     * @return el objeto o null
     * @throws JsonSyntaxException
     */
    public static <T> T loadObject(String key, Type objetType) throws JsonSyntaxException {
        return loadObject(key, objetType, new Gson());
    }

    /**
     * Carga desde persistencia un objeto a partir de su clave
     *
     * @param key      clave con la que se va a persistir
     * @param classOfT clase del objeto
     * @param <T>      tipo delo objeto
     * @param gson     objeto gson para parsear (no puede ser null)
     * @return el objeto o null
     * @throws JsonSyntaxException
     */
    public static <T> T loadObject(String key, Class<T> classOfT, Gson gson) throws JsonSyntaxException {
        return loadObject(key, classOfT, null, gson);
    }

    /**
     * Carga desde persistencia un objeto a partir de su clave
     *
     * @param key       clave con la que se va a persistir
     * @param objetType tipo del objeto
     * @param gson      objeto gson para parsear (no puede ser null)
     * @return el objeto o null
     * @throws JsonSyntaxException
     */
    public static <T> T loadObject(String key, Type objetType, Gson gson) throws JsonSyntaxException {
        return loadObject(key, null, objetType, gson);
    }

    /**
     * Carga desde persistencia un objeto a partir de su clave
     *
     * @param key      clave con la que se va a persistir
     * @param classOfT clase del objeto
     * @param gson     objeto gson para parsear (no puede ser null)
     * @return el objeto o null
     * @throws JsonSyntaxException
     */
    private static <T> T loadObject(String key, Class<T> classOfT, Type objetType, Gson gson) throws JsonSyntaxException {
        T object = null;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        String tokenJson = preferences.getString(key, null);
        if (tokenJson != null) {
            if (classOfT != null)
                object = gson.fromJson(tokenJson, classOfT);
            else if (objetType != null)
                object = gson.fromJson(tokenJson, objetType);
        }

        return object;
    }

    /**
     * Almacena un objeto en persistencia y le asocia una clave
     *
     * @param key clave de almacenamiento
     * @param o   objeto a persistir
     */
    public static void saveObject(String key, Object o) {
        saveObject(key, o, new Gson());
    }


    /**
     * Almacena un objeto en persistencia y le asocia una clave
     *
     * @param key  clave de almacenamiento
     * @param o    objeto a persistir
     * @param gson objeto gson para parsear (no puede ser null)
     */
    public static void saveObject(String key, Object o, Gson gson) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, gson.toJson(o));
        editor.commit();
    }

    /**
     * Elimina on objeto de persistencia
     *
     * @param key clave de almacenamiento
     */
    public static void deleteObject(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }
}